const path = require('path');
const express = require('express');
const app = express();

// Settings
app.set('port', process.env.PORT || 4000); // Si no hay un puerto establecido, usará el 3000

// Subir Archivos estáticos(html,php,etc)

//app.use(express.static(path.join(__dirname, 'public')));

//app.use(express.static(path.join(__dirname, 'index.html')));

/* Iniciar el Servidor */
const server = app.listen(app.get('port'), () => {
    console.log('Servidor conectado en el Puerto', app.get('port'));
});

/*WebSockets*/
const SocketIO = require('socket.io');
const io = SocketIO(server); // Toma la conexion del servidor

var jugadores = [];

var sala = [];

io.on('connection', (socket) => {

    console.log('New Connection', socket.id);
    /*var canal = 'Público';
    socket.join(canal);
    listaUsuario(socket);
    registrarJugador(socket);
    console.log(jugadores);
    Invitacion(socket);
    decisionInvitacion(socket);
    unirmeCanal(socket, canal);
    Jugada(socket);
    volverPublic(socket);*/

});

/*function idAleatorio(){
    var idAleatorio = Math.round(Math.random()*1000);
    return idAleatorio;
}*/

function Jugador(id, nombre) {
  this.id = id;
  this.nombre = nombre;
}

function listaUsuario(socket){

    io.sockets.emit('listaUsuario', {jugador: jugadores});
    //socket.broadcast.emit('listaUsuario', {jugador: jugadores});
}

function registrarJugador(socket){
  socket.on('nombreJugador', function(data){
    var nombreJugador = data.nombreJugador;
    console.log('El usuario '+ nombreJugador+ ' se conectó');
    if(jugadores.indexOf(nombreJugador) == -1){
        var jug = new Jugador(socket.id, nombreJugador);
        jugadores.push(jug);
        //jugadores.push(nombreJugador);
        //socket.nombreJugador = nombreJugador;
        console.log(jugadores);
        listaUsuario(socket);
        miUsuario(socket, jug);
    }else{
      console.log('No se pudo conectar');
       //socket.emit('Jugador en linea', {correcto: false, nick: nombreJugador});
    }
  })
}

function miUsuario(socket, jug){
    socket.emit('miUsuario', jug);
}

function disconnect(socket){
    socket.on('disconnect', function(e){
        socket.emit('disconnect', socket.id);
    })
}

function Invitacion(socket){
    socket.on('Invitacion', function(data){
        console.log(data.nombreRetador, 'retó a', data.nombreOponente, 'a un juego');
        //console.log('Respuesta de', data.nombreRetador, ':', data.resp);
        socket.broadcast.to(data.idOponente).emit('recibirInvitacion', data);
    });
}

function decisionInvitacion(socket){
  socket.on('decisionInvitacion', function(data){
      //socket.join(data.canal);
      console.log('Decisión de', data.nombreOponente,':', data.res);
      socket.broadcast.to(data.idRetador).emit('respuestaInvitacion', data);
  });
}

/*function rechazaJuego(socket){
    socket.on('rechazaJuego', function(data){
        //socket.join(data.canal);
        socket.broadcast.to(data.idRetador).emit('rechazaJuego', data);
    })
}

function aceptaJuego(socket){
    socket.on('aceptaJuego', function(data){
        //socket.join(data.canal);
        socket.broadcast.to(data.idRetador).emit('aceptaJuego', data);
    })
}*/

function unirmeCanal(socket, canal){
    socket.on('unirmeCanal', function(data){
        socket.leave(canal);
        socket.join(data.nuevoCanal);
        canal = data.nuevoCanal;
        socket.emit('unirmeCanal', data);
    });
}

function Jugada(socket){
  socket.on('Jugada', function(data){
      console.log('Canal:', data.canal);
      socket.broadcast.to(data.canal).emit('resultadoOponente', data);
  });
}

function volverPublic(socket){
    socket.on('volverPublic', function(data){
        socket.leave(data.viejoCanal);
        socket.join(data.canalNuevo);
        var canalPublico = data.canalNuevo;
        socket.emit('volverPublic', canalPublico);
    });
}
